englishAlphabet = "abcdefghijklmnopqrstuvwxyz";
norwegianAlphabet = "abcdefghijklmnopqrstuvwxyzæøå";

function findEveryUniqueLetter(string, alphabet){
    var lettersInString = []; 

    for(index = 0; index < alphabet.length; index++){ 
        var letter = alphabet.charAt(index); 
        var re = new RegExp(letter, "i"); 

        if(re.test(string)){ 
            lettersInString.push(letter); 
        } 
    }
    return lettersInString;     
}

//Tar inn to arrays og returner et array med differansen.
function arr_diff (a1, a2) {

    var a = [], diff = [];

    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }

    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        } else {
            a[a2[i]] = true;
        }
    }

    for (var k in a) {
        diff.push(k);
    }

    return diff;
}

function checkString(alphabet){

    var alphabets = ["abcdefghijklmnopqrstuvwxyz", "abcdefghijklmnopqrstuvwxyzæøå"]
    var alphabetLists = [["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"],["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","æ","ø","å"]]

    var string = document.getElementById('inputBox').value;
    var userConfig = parseInt(document.getElementById('languageList').value);
    var alphabet = alphabets[userConfig];
    var alphabetList = alphabetLists[userConfig];

    var diffSize = alphabet.length - findEveryUniqueLetter(string, alphabet).length;
    var diffArray = arr_diff(findEveryUniqueLetter(string, alphabet), alphabetList);
    console.log(diffArray);

    document.getElementById("remainingLettersDisplay").innerHTML = diffArray;
    
    document.getElementById("wordLength").innerHTML = "String length: " + string.length;
    
    if(userConfig === 1){
        document.getElementById("language").innerHTML = "Language: Norwegian"
    }

    if(userConfig === 0){
        document.getElementById("language").innerHTML = "Language: English"
    }

    if(findEveryUniqueLetter(string, alphabet).length === alphabet.length){
        document.getElementById("pangramBool").innerHTML = "<b>Pangram detected!</b>";
    } else {
        document.getElementById("pangramBool").innerHTML = "Remaining letters in alphabet: " + diffSize;
    }
}

